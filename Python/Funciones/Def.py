#Parametros
#def suma (x,y):    #"Suma" Es la variable; Lo que esta entre parentesis son los parametros.
  #  total = x+y    #Estos son los argumentos.
 #   print('El total de la suma de X e Y es ', total, '.')

#suma(10, 50)       #Se puede invocar tantas veces queramos.


# -----------------------------------------------------
##Parametros default.

def login (User='root', Password='123'):
    print('Usuario: ', User)
    print('Password: ', Password)

login('', 1234)


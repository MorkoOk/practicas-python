def lista_De_Argumento(*lista): #Cuando una funcion puede recibir una lista de parametros (listas o tuplas)
    print(lista)

def lista_de_argumentos_asociativa(**diccionario): #Cuando una funcion tiene parametros nombrados Ej: uno=1 (diccionario)
    print(diccionario)

def arguments (*args, **kwargs):
    print(args)
    print(kwargs)

lista_De_Argumento(1,2,3,4,5,6)
lista_De_Argumento('Uno', 'dos', 'tres', 'cuatro')

lista_de_argumentos_asociativa(a=1,b=2,c=3,d=4)
lista_de_argumentos_asociativa(uno=1,dos=2,tres=3,cuatro=4)

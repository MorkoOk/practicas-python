'''

lista = [10,20] #10 y 20 estan empaquetados, estos mismos se pueden desempaquetar.
def func (a,b):
    print()

func(*lista)
func(10,20)             #formas de ver el desempaquetamiento, linea 5,6,7
func(a=10,b=20)

'''

def persona(nombre,apellido, edad):
    print(nombre)
    print(apellido)
    print(edad)

#t = 'Gonzalo', 'Romero', '19' #lista.
persona(t[0], t[1], t[2]) #tupla.
#persona(*t) #invocacion
d={'nombre':'Gonzalo','apellido':'Romero','edad':'19'} #diccionario
persona(**d)
#ordenar una lista.
'''
lista = [11,10,12]

def func(a,b,c):
    print(a)
    print(b)
    print(c)

lista.sort()  #reordena una lista con .sort()
func(*lista)  #Se invoca la lista.
'''

#######
'''
#ordenar una tupla

tupla = (11,10,12)
#diccionario = {11,10,12}

def func(a,b,c):
    print(a)
    print(b)
    print(c)

l = [*tupla] #se tranforma la tupla a una lista.
l.sort()     #Se usa la funcion sort.
func(*l)     #Se acomoda los datos.

'''
#diccionario = {11,10,12}


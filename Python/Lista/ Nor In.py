#Verifica si el valor se encuentra en la lista.

#x in [] #lista
#x in () #tupla
#x in {} #diccionario

x = [1, 3, 4, 6, 8, 15]
#contenido
if 1 in x:
    print("contenido")
else:
    print("No est'a contenido")
if 2 in x:
    print("contenido")
else:
    print("No est'a contenido")
if 15 in x:
    print("contenido")
else:
    print("No est'a contenido")


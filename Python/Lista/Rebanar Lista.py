#Nomenclatura

#     0   1   2   3   4   5

#     P   Y   T   H   O   N

#     -6  -5  -4  -3  -2  -1
"""
lista = "bienvenido_al_curso_de_python"
>>> lista
'bienvenido_al_curso_de_python'
>>> lista [10]
'_'
>>> lista[-2]
'o'
>>> lista[2]
'e'
>>> lista[20]
'd'
>>> lista[:20]
'bienvenido_al_curso_'
>>> lista[10:20]
'_al_curso_'
>>> lista[::2]
'bevnd_lcrod_yhn'
>>> lista[::3]
'bnnoluoeyo'
>>> lista[-]
SyntaxError: invalid syntax
>>> lista[::-1]
'nohtyp_ed_osruc_la_odinevneib'
>>> lista = ['Claudio', 'Jose', 'Maria', 'Beltrano', 'Juan', 'Fulano', 'Mengano']
>>> lista
['Claudio', 'Jose', 'Maria', 'Beltrano', 'Juan', 'Fulano', 'Mengano']
>>> lista.reverse ()
>>> lista
['Mengano', 'Fulano', 'Juan', 'Beltrano', 'Maria', 'Jose', 'Claudio']
>>> lista.reverse ()
>>> lista
['Claudio', 'Jose', 'Maria', 'Beltrano', 'Juan', 'Fulano', 'Mengano']
>>> lista.sort()
>>> lista
['Beltrano', 'Claudio', 'Fulano', 'Jose', 'Juan', 'Maria', 'Mengano']
>>> lista.sort(reverse=True)
>>> lista
['Mengano', 'Maria', 'Juan', 'Jose', 'Fulano', 'Claudio', 'Beltrano']
>>> 
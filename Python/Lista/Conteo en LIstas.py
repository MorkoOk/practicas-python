>>> lista = ['Claudio', 'Jose', 'Maria', 'Beltrano', 'Juan', 'Fulano', 'Mengano']
>>> lista
['Claudio', 'Jose', 'Maria', 'Beltrano', 'Juan', 'Fulano', 'Mengano']
>>> len(lista)
7
>>> lista[len(lista)-1]
'Mengano'
>>> lista[len(lista)0]
SyntaxError: invalid syntax
>>> lista[len(lista) 0 ]
SyntaxError: invalid syntax
>>> lista[len(lista)-7]
'Claudio'
>>> lista[0]
'Claudio'
>>> lista = lista + [Jose][5]
Traceback (most recent call last):
  File "<pyshell#8>", line 1, in <module>
    lista = lista + [Jose][5]
NameError: name 'Jose' is not defined
>>> lista.insert (5, Jose)
Traceback (most recent call last):
  File "<pyshell#9>", line 1, in <module>
    lista.insert (5, Jose)
NameError: name 'Jose' is not defined
>>> lista.insert (5, 'Jose')
>>> lista
['Claudio', 'Jose', 'Maria', 'Beltrano', 'Juan', 'Jose', 'Fulano', 'Mengano']
>>> lista.count['Jose']
Traceback (most recent call last):
  File "<pyshell#12>", line 1, in <module>
    lista.count['Jose']
TypeError: 'builtin_function_or_method' object is not subscriptable
>>> lista.count('Jose')
2
>>> lista.index('Maria')
2
>>> lista.index('Jose')
1